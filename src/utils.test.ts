import { getViewpointURL, indexBy } from "./utils";

test("indexBy should properly index plain objects arrays", () => {
  expect(
    indexBy<{ id: string; name: string }>(
      [
        { name: "John Doe", id: "p1" },
        { name: "John Wayne", id: "p2" },
        { name: "John Wick", id: "p3" },
        { name: "John Williams", id: "p4" },
      ],
      "id",
    ),
  ).toStrictEqual({
    p1: { name: "John Doe", id: "p1" },
    p2: { name: "John Wayne", id: "p2" },
    p3: { name: "John Wick", id: "p3" },
    p4: { name: "John Williams", id: "p4" },
  });
});

test("indexBy should only keep the first matching element, when keys collide", () => {
  expect(
    indexBy<{ id: string; name: string }>(
      [
        { name: "John Doe", id: "p1" },
        { name: "John Doe 2", id: "p1" },
        { name: "John Wayne", id: "p2" },
        { name: "John Wick", id: "p3" },
        { name: "John Williams", id: "p4" },
        { name: "John Williams 2", id: "p4" },
      ],
      "id",
    ),
  ).toStrictEqual({
    p1: { name: "John Doe", id: "p1" },
    p2: { name: "John Wayne", id: "p2" },
    p3: { name: "John Wick", id: "p3" },
    p4: { name: "John Williams", id: "p4" },
  });
});

test("indexBy should only keep the first matching element, when keys collide", () => {
  expect(
    indexBy<{ id: string; name: string }>(
      [
        { name: "John Doe", id: "p1" },
        { name: "John Doe 2", id: "p1" },
        { name: "John Wayne", id: "p2" },
        { name: "John Wick", id: "p3" },
        { name: "John Williams", id: "p4" },
        { name: "John Williams 2", id: "p4" },
      ],
      "id",
    ),
  ).toStrictEqual({
    p1: { name: "John Doe", id: "p1" },
    p2: { name: "John Wayne", id: "p2" },
    p3: { name: "John Wick", id: "p3" },
    p4: { name: "John Williams", id: "p4" },
  });
});

test("getViewpointURL should work properly", () => {
  expect(getViewpointURL({ id: "toto" })).toBe("/vp/toto");
  expect(getViewpointURL({ id: "toto" }, { a: 1, b: 2, c: "123" })).toBe("/vp/toto?a=1&b=2&c=123");
  expect(getViewpointURL({ id: "toto" }, { a: "=" })).toBe("/vp/toto?a=%3D");
});
