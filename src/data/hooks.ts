import { useState, useEffect } from "react";
import { Table } from "./datasources";
import { PlainObject } from "../utils";

export function useTable(
  table: Table,
  params: PlainObject
): { loading: boolean; error: Error | null; data: Array<any> | null; refetch: () => void } {
  const [loading, setIsLoading] = useState<boolean>(true);
  const [error, setError] = useState<Error | null>(null);
  const [data, setData] = useState<Array<any> | null>(null);

  const refetch = async () => {
    // load the table
    try {
      const data = await table.load(params);
      setData(data);
    } catch (e) {
      setError(e);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    refetch();
  }, []);

  return {
    loading,
    error,
    data,
    refetch,
  };
}

interface PromiseDataType {
  name: string;
  data: Array<any> | null;
}

export function useData(tablesMap: PlainObject<Table>, params: PlainObject): any {
  const [loading, setIsLoading] = useState<boolean>(true);
  const [error, setError] = useState<Error | null>(null);
  const [data, setData] = useState<{ [key: string]: Array<any> | null }>({});

  const refetch = async () => {
    const promises: Array<Promise<PromiseDataType>> = Object.keys(tablesMap).map((tablename) => {
      return new Promise((resolve, reject) => {
        tablesMap[tablename]
          .load(params)
          .then((data) => {
            resolve({ name: tablename, data: data });
          })
          .catch((e) => {
            reject(e);
          });
      });
    });

    try {
      const results = await Promise.all(promises);
      const data: { [key: string]: Array<any> | null } = {};
      results.forEach((promise) => {
        data[promise.name] = promise.data;
      });
      setData(data);
    } catch (e) {
      setError(e);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    refetch();
  }, []);

  return {
    loading,
    error,
    data,
    refetch,
  };
}
