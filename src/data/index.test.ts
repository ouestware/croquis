import { configuration } from "../config";
import { DATASOURCE_TYPES } from "./datasources";

test("Load configuration", () => {
  configuration.datasources.forEach((dsConfig) => {
    const ds = new DATASOURCE_TYPES[dsConfig.type](dsConfig);
    expect(ds).toBeDefined();
  });
});
