import { Table, TableConfiguration } from "./table";

// Common datasource configuration (if needed one day)
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface DataSourceConfiguration {}

// Definition for DataSource construtor
export interface DataSourceConstructor {
  new (config: any): DataSource<any, any>;
}

/**
 * Abstract class for DataSources
 */
export abstract class DataSource<M extends DataSourceConfiguration, N extends TableConfiguration> {
  config: M;

  /**
   * Default constructor.
   *
   * @param config Configuration object of the datasource
   */
  constructor(config: M) {
    this.config = config;
  }

  /**
   * You should implement this method to let us know how to build
   * a "Table" for this datasource, base on the table configuration.
   *
   * @param {TableConfiguration} config the configuration to generate a table
   * @return {Table} The Nable
   */
  abstract buildTable(config: N): Table;
}
