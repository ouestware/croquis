import { TableConfiguration } from "../table";

/**
 * CSV Table configuration.
 */
interface CsvTableConfiguration extends TableConfiguration {
  url: string;
  delimiter: string;
  encoding?: string;
  header?: boolean;
}

export default CsvTableConfiguration;
