import { parse } from "papaparse";
import { DataSource } from "../datasource";
import { Table } from "../table";
import DataSourceConfiguration from "./config.datasource";
import TableConfiguration from "./config.table";
import { PlainObject } from "../../../utils";
/**
 * CSV data source : helps you to create a table from a CSV.
 */
export default class CsvDataSource extends DataSource<DataSourceConfiguration, TableConfiguration> {
  /**
   * Load the CSV specified in the configuration as a Table.
   */
  buildTable(config: TableConfiguration): Table {
    const load = () => {
      return new Promise<Array<PlainObject>>((resolve, reject) => {
        // TODO: make the url with the params for API??
        // Can be useful for json api
        parse(config.url, {
          download: true,
          header: config.header || true,
          delimiter: config.delimiter,
          error: (error) => reject(error),
          complete: (result) => resolve(result.data as Array<PlainObject>),
        });
      });
    };
    return new Table(load);
  }
}
