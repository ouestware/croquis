import { DataSourceConfiguration } from "../datasource";

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface CsvDataSourceConfiguration extends DataSourceConfiguration {}

export default CsvDataSourceConfiguration;
