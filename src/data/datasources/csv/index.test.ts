import CsvDataSource from "./index";
import { Table } from "../table";

test("Build CSV Table", async () => {
  // create the datasource
  const dsCSV = new CsvDataSource({
    id: "remote_csv",
  });

  // create the table
  const table: Table = dsCSV.buildTable({
    url: "https://raw.githubusercontent.com/mholt/PapaParse/master/docs/resources/files/normal.csv",
    delimiter: ",",
    encoding: "",
  });

  //  load the data
  const data = await table.load({});

  // Checks
  expect(data.length).toBe(1135);
  expect(data[0]["CONTENT TYPE"]).toBe("Journals");
  expect(data[0]["TITLE"]).toBe("ACM Computing Surveys ");
});
