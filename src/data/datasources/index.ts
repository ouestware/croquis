import { DataSourceConstructor } from "./datasource";
// import of DataSources
import CsvDataSource from "./csv/index";
import CsvDataSourceConfig from "./csv/config.datasource";
import CsvTableConfig from "./csv/config.table";
import Neo4jDataSource from "./neo4j/index";
import Neo4jDataSourceConfig from "./neo4j/config.datasource";
import Neo4jTableConfig from "./neo4j/config.table";

export { Table } from "./table";
export { DataSource } from "./datasource";

// Export the list of available datasource type
export const DATASOURCE_TYPES: { [id: string]: DataSourceConstructor } = {
  Neo4j: Neo4jDataSource,
  CSV: CsvDataSource,
};

// Export the DataSourceConfiguration type.
export type DataSourcesConfigurationType = Neo4jDataSourceConfig | CsvDataSourceConfig;

// Export the tableConfiguration type.
export type TablesConfigurationType = Neo4jTableConfig | CsvTableConfig;
