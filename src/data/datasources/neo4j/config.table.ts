import { TableConfiguration } from "../table";

/**
 * Table configuration for Neo4j DataSource.
 * It's just a cypher query.
 * It's parameters are the one from the view-point
 */
export interface Neo4jTableConfiguration extends TableConfiguration {
  query: string;
}

export default Neo4jTableConfiguration;
