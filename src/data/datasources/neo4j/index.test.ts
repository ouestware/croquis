import Neo4jDataSource from "./index";
import { Table } from "../../table";

test("[INTEG] Build Neo4j Table", async () => {
  // create the datasource
  const dsNeo4j = new Neo4jDataSource({
    id: "local_neo4j",
    url: "bolt://localhost",
    login: "neo4j",
    password: "admin",
  });

  // create the table
  const table: Table = dsNeo4j.buildTable({ id: "test_table", query: "RETURN 1 as col1, 2 as col2" });

  // load the data
  const data: Array<any> = await table.load();

  // checks
  expect(data.length).toBe(1);
  expect(data[0].col1).toBe(1);
  expect(data[0].col2).toBe(2);
});
