import { DataSourceConfiguration } from "../datasource";

/**
 * Configuration for the Neo4j datasource.
 */
export interface Neo4jDataSourceConfiguration extends DataSourceConfiguration {
  url: string;
  login: string;
  password: string;
}

export default Neo4jDataSourceConfiguration;
