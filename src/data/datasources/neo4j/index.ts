import * as neo4j from "neo4j-driver";
import { DataSource } from "../datasource";
import { Table } from "../table";
import DataSourceConfiguration from "./config.datasource";
import TableConfiguration from "./config.table";

/**
 * The Neo4j DataSource.
 */
export default class Neo4jDataSource extends DataSource<DataSourceConfiguration, TableConfiguration> {
  // The neo4j driver
  driver: neo4j.Driver;

  /**
   * Default constructor.
   *
   * @param config The DataSource configuration
   */
  constructor(config: DataSourceConfiguration) {
    super(config);
    this.driver = neo4j.driver(config.url, neo4j.auth.basic(config.login, config.password), {
      disableLosslessIntegers: true,
    });
  }

  /**
   * Build a table based on the query specified in the configuration.
   *
   * @param config The table configuration
   */
  buildTable(config: TableConfiguration): Table {
    const load = (params: { [key: string]: any }) => {
      return new Promise<Array<{ [key: string]: any }>>((resolve, reject) => {
        const session: neo4j.Session = this.driver.session({ defaultAccessMode: neo4j.session.READ });
        const result: Array<{ [key: string]: any }> = [];
        session.run(config.query, params).subscribe({
          onNext: (record: neo4j.Record) => {
            const item: any = {};
            try {
              record.forEach((value, key) => {
                item[key] = this.convertObjectDriverToJs(value);
              });
              result.push(item);
            } catch (e) {
              reject(e);
            }
          },
          onCompleted: () => {
            session.close();
            resolve(result);
          },
          onError: (error: neo4j.Neo4jError) => {
            session.close();
            reject(error);
          },
        });
      });
    };
    return new Table(load);
  }

  private convertObjectDriverToJs(value: any): any {
    //const result: any = value;
    return value;
  }
}
