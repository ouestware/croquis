import * as objectHash from "object-hash";
import { PlainObject } from "../../utils";

// Common table configuration (if needed one day)
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface TableConfiguration {}

/**
 * Table class.
 */
export class Table {
  // the function to load the table from the DataSource
  fctLoad: (params: PlainObject) => Promise<Array<any>>;

  // Caching object for the table. We have the data for the table,
  // and the hash of the parameters that permit to generate it
  cache: { data: Array<any>; hash: string };

  /**
   * Default constructor.
   *
   * @param load The loading function from the datasource
   */
  constructor(load: (params: PlainObject) => Promise<Array<any>>) {
    this.fctLoad = load;
    this.cache = { data: [], hash: "" };
  }

  /**
   * Method that loads/retrieves the data of the table.
   * It performs a call to the datasource if needed except if we already have the result
   * in the cache.
   *S
   * @param params The params of the view-point
   * @return The content of the table
   * @throw An error if the datasource failed to load the data
   */
  async load(params: PlainObject): Promise<Array<any>> {
    const hash: string = objectHash.sha1(params || {});
    if (this.cache.hash !== hash) {
      try {
        this.cache.data = await this.fctLoad(params);
        this.cache.hash = hash;
      } catch (e) {
        throw e;
      }
    }
    return this.cache.data;
  }

  /**
   * Compute the header of the table, based on the first row in the result.
   *
   * @return The header of the tbale (ie. column definition)
   * @throw An error if the table is not already loaded.
   */
  columns(): Array<{ name: string; type: string }> {
    if (!this.cache.data || this.cache.data.length < 1) {
      throw new Error(`Can't compute columns, there is no data`);
    }
    return Object.keys(this.cache.data[0]).map((key) => {
      return {
        name: key,
        type: typeof this.cache.data[0][key],
      };
    });
  }
}
