import { configuration } from "../config";
import { DATASOURCE_TYPES, DataSource, Table } from "./datasources";
import { DataSourceDefinition, TableDefinition } from "../types";

/**
 * This factory helps you to build/retrieve `DataSource` and `DataTable`.
 */
export class DataFactory {
  // List of datasource instances by their `id`
  private static datasources: { [id: string]: DataSource<any, any> } = {};

  /**
   * Retrieve a datasource by its `id`.
   * If the datasource is not already build, the function do it.
   *
   * @param {string} id The identifier of the datasource in the configuration
   * @return {DataSource} The corresponding `DataSource` instance.
   * @throw {Error} If the datasource ID is not found in the configuration
   * or if the datasource type is not found
   */
  static getDataSource(id: string): DataSource<any, any> {
    // if the datasource is not already build
    if (!this.datasources[id]) {
      // find the datasource definition
      const datasourceDef: DataSourceDefinition | undefined = configuration.datasources.find((def) => def.id === id);
      if (!datasourceDef) {
        throw new Error(`DataSource ${id} not found`);
      }

      // construct the datasource
      if (!DATASOURCE_TYPES[datasourceDef.type]) {
        throw new Error(`DataSourceType ${datasourceDef.type} not found`);
      }
      this.datasources[id] = new DATASOURCE_TYPES[datasourceDef.type](datasourceDef);
    }

    return this.datasources[id];
  }

  /**
   * Retrieve a table from its `id`.
   *
   * @param {string} id The identifier of the table in the configuration
   * @return {Table} The corresponding `Table` instance.
   * @throw {Error} If the table ID is not found in the configuration
   */
  static getTable(id: string): Table {
    // find the table definition
    const tableDef: TableDefinition | undefined = configuration.tables.find((def) => def.id === id);
    if (!tableDef) {
      throw new Error(`Table ${id} not found`);
    }

    // call the buildTable from the datasource
    return this.getDataSource(tableDef.datasource).buildTable(tableDef);
  }
}
