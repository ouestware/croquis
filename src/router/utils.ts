import { pickBy } from "lodash";
import { PartialButFor, PlainObject } from "../utils";
import { ParameterType, ViewPointDefinition } from "../types";

/**
 * This helper takes a string value (should come from URL query params) and a
 * parameter type, and returns a properly casted value if possible, and `null`
 * else. Also, it trims strings before casting.
 *
 * @param value The string value to cast.
 * @param type  The related parameter type.
 * @returns     A casted value in the specified type if possible, `null` else.
 */
export function castValue(value: string, type: ParameterType): any {
  const trimmed = value.trim();

  switch (type) {
    case "number":
      return isNaN(+trimmed) ? null : +trimmed;
    case "integer":
      const casted = +trimmed;
      return trimmed === casted + "" && casted === (casted | 0) ? casted : null;
    case "string":
      return trimmed;
  }

  return null;
}

/**
 * This helper takes a viewpoint and the dictionary of URL query params, removes
 * the irrelevant params and validates / casts the remaining params to their
 * proper type.
 *
 * @param params    The dictionary of query params, as they appear in the URL.
 * @param viewpoint The currently selected viewpoint.
 * @returns         The cleaned dictionary of viewpoint parameter values.
 */
export function cleanQueryParams(
  params: PlainObject<string>,
  viewpoint: PartialButFor<ViewPointDefinition, "parameters">,
): PlainObject {
  return pickBy(
    viewpoint.parameters.reduce(
      (iter, def) => (params[def.id] ? { ...iter, [def.id]: castValue(params[def.id], def.type) } : iter),
      {},
    ),
  );
}

/**
 * This helper generates a unique fingerprint string for each parameters and
 * viewpoint pair. It helps identifying when a user changes the current page.
 *
 * @param viewpoint The selected viewpoint.
 * @param params    The cleaned / casted values dictionary
 */
export function getFingerprint(
  viewpoint: PartialButFor<ViewPointDefinition, "id" | "parameters">,
  params: PlainObject,
): string {
  return [escape(viewpoint.id), viewpoint.parameters.map(({ id }) => escape(id) + "=" + escape(params[id] || ""))].join(
    "&",
  );
}
