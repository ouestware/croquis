import { castValue, cleanQueryParams, getFingerprint } from "./utils";
import { PartialButFor } from "../utils";
import { ViewPointDefinition } from "../types";

test("castValue should trim values and cast when possible", () => {
  expect(castValue("  toto   ", "string")).toBe("toto");
  expect(castValue(" 123 ", "integer")).toBe(123);
  expect(castValue(" 123.456 ", "number")).toBe(123.456);

  expect(castValue(" 123.456 ", "integer")).toBe(null);
  expect(castValue(" huhu ", "integer")).toBe(null);
});

test("cleanQueryParams should clean unspecified parameters", () => {
  expect(
    cleanQueryParams({ toto: "abc", tutu: "def" }, { parameters: [{ id: "tutu", type: "string" }] }),
  ).toStrictEqual({
    tutu: "def",
  });

  expect(cleanQueryParams({ toto: "abc", tutu: "def" }, { parameters: [] })).toStrictEqual({});
});

test("cleanQueryParams should properly cast numbers", () => {
  expect(
    cleanQueryParams(
      { size: "12.17", birthyear: "1880" },
      {
        parameters: [
          { id: "size", type: "number" },
          { id: "birthyear", type: "integer" },
        ],
      },
    ),
  ).toStrictEqual({
    size: 12.17,
    birthyear: 1880,
  });
});

test("cleanQueryParams should flush uncastable values", () => {
  expect(
    cleanQueryParams(
      { year1: "abc", year2: "1880.2" },
      {
        parameters: [
          { id: "year1", type: "number" },
          { id: "year2", type: "integer" },
        ],
      },
    ),
  ).toStrictEqual({});
});

test('getFingerprint should generate unique strings for each viewpoint "page"', () => {
  // Irrelevant values are not considered:
  const vp1 = { id: "vp1", parameters: [] };
  expect(getFingerprint(vp1, { a: 1 })).toBe(getFingerprint(vp1, { a: 2 }));

  // Param values "order" is not considered:
  const vp2: PartialButFor<ViewPointDefinition, "id" | "parameters"> = {
    id: "vp2",
    parameters: [
      { id: "a", type: "string" },
      { id: "b", type: "string" },
    ],
  };
  expect(getFingerprint(vp2, { a: "123", b: "456" })).toStrictEqual(getFingerprint(vp2, { b: "456", a: "123" }));

  // Param values are considered:
  expect(getFingerprint(vp2, { a: "123", b: "456" })).not.toBe(getFingerprint(vp2, { a: "123", b: "789" }));

  // Viewpoint IDs are considered:
  const vp3 = { ...vp1, id: "vp3" };
  expect(getFingerprint(vp1, {})).not.toBe(getFingerprint(vp3, {}));
});
