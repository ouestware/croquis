import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";

// Definition of a route
export interface RouteDefinition {
  path: string;
  redirect?: string;
  component?: any;
  exact?: boolean;
  routes?: RouteDefinition[];
}

interface Props {
  path?: string;
  routes: RouteDefinition[];
}

export const RouterWrapper: React.FC<Props> = (props: Props) => {
  // let history = useHistory();
  // history.listen((location, action) => {
  //   console.log(`The current URL is ${location.pathname}${location.search}${location.hash}`);
  //   console.log(`The last navigation action was ${action}`);
  // });
  /**
   * Function that flatten tree routes recursively.
   */
  const flattenRoutes = (path = "", routes: RouteDefinition[] = []): Array<RouteDefinition> => {
    // The result
    let routesList: Array<RouteDefinition> = [];
    routes.forEach((route: RouteDefinition) => {
      // construct the route definition
      const currentRoute: RouteDefinition = {
        path: `${path}${route.path}`,
      };
      if (route.redirect) {
        currentRoute.redirect = route.redirect;
      }
      if (route.component) {
        currentRoute.component = route.component;
      }
      if (route.exact) {
        currentRoute.exact = route.exact;
      }
      // recursivity
      if (route.routes) {
        routesList = routesList.concat(flattenRoutes(currentRoute.path, route.routes));
      }
      routesList.push(currentRoute);
    });
    return routesList;
  };
  const routesFlatten = flattenRoutes(props.path, props.routes);

  if (routesFlatten.length > 0)
    return (
      <Switch>
        {routesFlatten.map((route: RouteDefinition) => {
          if (route.redirect) {
            return (
              <Redirect
                key={`${route.path}`}
                exact={!!route.exact}
                path={`${route.path}`}
                to={`${route.redirect}`}
              />
            );
          } else {
            return (
              <Route
                key={`${route.path}`}
                path={`${route.path}`}
                exact={!!route.exact}
                render={(props) => <route.component {...props.match.params} />}
              />
            );
          }
        })}
      </Switch>
    );
  else return null;
};
