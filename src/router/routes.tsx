import React from "react";
import * as queryString from "query-string";
import { useLocation } from "react-router";

import { RouteDefinition } from "./router";
import { HomePage } from "../page/homepage";
import { ViewPointPage } from "../page/viewpointpage";
import { configuration } from "../config";
import { PlainObject } from "../utils";
import { cleanQueryParams, getFingerprint } from "./utils";

// TODO: Build the routes from the configuration to have better url ??
// for now, it's a generic PV url
export const routes: RouteDefinition[] = [
  {
    path: "",
    redirect: "/",
    routes: [
      {
        path: "/",
        exact: true,
        component: (() => <HomePage configuration={configuration} />) as React.FC,
      },
      {
        path: "/vp/:viewpointId",
        exact: true,
        component: (({ viewpointId }: { viewpointId: string }) => {
          const location = useLocation();
          const viewpoint = configuration.viewpoints.find((vp) => vp.id === viewpointId);
          if (!viewpoint) return null;

          const queryParams = queryString.parse(location.search) as PlainObject<string>;
          const params = cleanQueryParams(queryParams, viewpoint);

          return (
            <ViewPointPage
              configuration={configuration}
              key={getFingerprint(viewpoint, params)}
              viewpoint={viewpoint}
              params={params}
            />
          );
        }) as React.FC,
      },
    ],
  },
];
