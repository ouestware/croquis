import { DataSourceDefinition, TableDefinition, ViewPointDefinition } from "../types";

export interface Configuration {
  metadata?: {
    title?: string;
    subtitle?: string;
    description?: string;
  };
  datasources: Array<DataSourceDefinition>;
  tables: Array<TableDefinition>;
  viewpoints: Array<ViewPointDefinition>;
}

// eslint-disable no-template-curly-in-string
export const configuration: Configuration = {
  metadata: {
    title: "Croquis",
    subtitle: "A sample datascape to explore Croquis's capabilities",
  },
  datasources: [
    {
      id: "myLocalNeo4j",
      url: "bolt://localhost",
      login: "neo4j",
      password: "admin",
      type: "Neo4j",
    },
    {
      id: "myCsvFile",
      type: "CSV",
    },
  ],
  tables: [
    {
      id: "count_per_label",
      datasource: "myLocalNeo4j",
      query: `
        CALL db.labels() YIELD label
        WITH label
        CALL apoc.cypher.run('MATCH (n:\`' + label + '\`)
        RETURN count(*) AS count', {}) YIELD value
        WHERE value.count > 0
        RETURN label, value.count AS count`,
    },
    {
      id: "my_csv",
      datasource: "myCsvFile",
      url: "https://raw.githubusercontent.com/mholt/PapaParse/master/docs/resources/files/normal.csv",
      delimiter: ",",
    },
  ],
  viewpoints: [
    {
      id: "trading-country",
      title: "Trading balance for ${country} on the period ${year1}-${year2}",
      image: "http://ricardo.medialab.sciences-po.fr/img/home_source_details.png",
      description:
        "Dans ce point de vue, le commerce total est égal à la somme des flux exportations (importations) avec toutes les entités partenaires.",
      parameters: [
        { id: "country", label: "Pays", type: "string", required: true },
        { id: "year1", label: "Année de départ", type: "number", required: true, minimum: 1800, maximum: 2020 },
        { id: "year2", label: "Année de fin", type: "number", required: true, minimum: 1800, maximum: 2020 },
      ],
      components: [
        {
          id: "network",
          title: "Network trading between countries",
          tables: [{ id: "my_csv", alias: "table" }],
          action: (e) => {
            console.log("action", e);
          },
          type: "datagrid",
          options: {
            pagination: true,
          },
        },
        {
          id: "chart",
          title: "Neo4j label chart",
          tables: [{ id: "count_per_label" }],
          type: "vega",
          options: {
            spec: {
              data: { name: "count_per_label" },
              description: "A simple bar chart with embedded data.",
              encoding: {
                x: { field: "label", type: "ordinal" },
                y: { field: "count", type: "quantitative" },
              },
              mark: "bar",
            },
          },
          action: (e) => {
            console.log("action", e);
          },
        },
      ],
    },
  ],
};
