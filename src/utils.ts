import { mapValues, groupBy } from "lodash";
import { ViewPointDefinition } from "./types";

/**
 * Returns a type similar to T, but with the the K set of properties of the type
 * T optional.
 */
export type PartialBy<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;

/**
 * Returns a type similar to T, but with the the K set of properties of the type
 * T *required*, and the rest optional.
 */
export type PartialButFor<T, K extends keyof T> = Pick<T, K> & Partial<Omit<T, K>>;

/**
 * Util type to represent maps of typed elements, but implemented with
 * JavaScript objects.
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type PlainObject<T = any> = { [k: string]: T };

/**
 * A short helper to index a collection of values on a given key.
 *
 * Example:
 * ********
 * > indexBy([
 *     { name: "John Doe", id: "p1" },
 *     { name: "John Wayne", id: "p2" },
 *     { name: "John Wick", id: "p3" },
 *     { name: "John Williams", id: "p4" },
 *   ]);
 * // would return:
 * > {
 *     p1: { name: "John Doe", id: "p1" },
 *     p2: { name: "John Wayne", id: "p2" },
 *     p3: { name: "John Wick", id: "p3" },
 *     p4: { name: "John Williams", id: "p4" },
 *   }
 *
 * If multiple elements have the same value for the given key, only the first
 * one is indexed.
 *
 * @param array An array of elements, supposed to have different required values
 *              for the given key.
 * @param key   A string key.
 * @returns     A plain JS object, with values being elements from the input
 *              array.
 */
export function indexBy<T>(array: T[], key: keyof T): PlainObject<T> {
  return mapValues(groupBy(array, key), (values) => values[0]);
}

/**
 * Given a viewpoint definition and possibly a params plain object, returns the
 * related relative URL.
 *
 * TODO:
 * *****
 * Deal with proper params data casting (strings, numbers, arrays...)
 *
 * @param viewpoint A complete viewpoint definition.
 * @param params    An optional params plain object.
 * @returns         A relative URL.
 */
export function getViewpointURL(viewpoint: PartialButFor<ViewPointDefinition, "id">, params?: PlainObject): string {
  const keys = Object.keys(params || {});

  return params && keys.length
    ? `/vp/${viewpoint.id}?` +
        keys.map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`).join("&")
    : `/vp/${viewpoint.id}`;
}
