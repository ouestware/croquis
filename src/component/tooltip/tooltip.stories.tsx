import React from "react";
import { flatten, times, constant } from "lodash";

import { Tooltip } from "./tooltip";

import "../../scss/index.scss";

export default { title: "Tooltip" };

const config = {
  viewpoints: [
    {
      id: "country-trade",
      title: "Commerce total de ${country} de ${startDate} à ${endDate}",
      components: [],
      parameters: [
        { id: "country", label: "Pays", type: "string" },
        { id: "startYear", label: "Année de début", type: "integer" },
        { id: "endYear", label: "Date de fin", type: "integer" },
      ],
    },
    {
      id: "world-trade",
      title: "Commerce mondial de ${startDate} à ${endDate}",
      components: [],
      parameters: [
        { id: "startYear", label: "Année de début", type: "integer" },
        { id: "endYear", label: "Date de fin", type: "integer" },
      ],
    },
  ],
};

const internalLinks = [
  {
    viewpointId: "country-trade",
    params: {
      country: "France",
      startDate: 1870,
      endDate: 1924,
    },
  },
  {
    viewpointId: "country-trade",
    params: {
      country: "Angleterre",
      startDate: 1870,
      endDate: 1924,
    },
  },
  {
    viewpointId: "world-trade",
    params: {
      startDate: 1870,
      endDate: 1924,
    },
  },
];

const externalLinks = [
  {
    label: "Wikipedia | Transport fluvial",
    url: "https://fr.wikipedia.org/wiki/Transport_fluvial",
  },
  {
    label: "Wikipedia | Transport fluvial en France",
    url: "https://fr.wikipedia.org/wiki/Transport_fluvial_en_France",
  },
  {
    label: "Wikipedia | Seconde Guerre Mondiale",
    url: "https://fr.wikipedia.org/wiki/Seconde_Guerre_mondiale",
  },
];

export const basicExample = () => (
  <Tooltip config={config} internalLinks={internalLinks} externalLinks={externalLinks} />
);

export const longExample = () => (
  <Tooltip
    config={config}
    internalLinks={flatten(times(5, constant(internalLinks)))}
    externalLinks={flatten(times(5, constant(externalLinks)))}
  />
);
