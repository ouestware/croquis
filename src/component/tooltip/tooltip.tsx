import React from "react";
import { useHistory } from "react-router-dom";
import { Trans } from "react-i18next";

import { ExternalLinkDefinition, InternalLinkDefinition, ViewPointDefinition } from "../../types";
import { getViewpointURL, indexBy, PartialButFor } from "../../utils";
import { Configuration } from "../../config";
import { Title } from "../title";

export type TooltipProps = {
  internalLinks: InternalLinkDefinition[];
  externalLinks: ExternalLinkDefinition[];
  config: PartialButFor<Configuration, "viewpoints">;
};

export const Tooltip: React.FC<TooltipProps> = (props: TooltipProps) => {
  const history = useHistory();

  const viewpointsIndex = indexBy<ViewPointDefinition>(props.config.viewpoints, "id");

  function gotoViewPoint(link: InternalLinkDefinition): void {
    const url = getViewpointURL(viewpointsIndex[link.viewpointId], link.params);

    // For cases where history isn't properly initialized
    if (!history)
      console.log(`[Tooltip] Opening viewpoint ${link.viewpointId} with data`, link.params, `(url: ${url})`);
    else history.push(url);
  }

  return (
    <article className="c-tooltip">
      {(props.internalLinks || []).length > 1 ? (
        <section className="c-tooltip-group">
          <h4 className="c-tooltip-list-title"><Trans>Go to...</Trans></h4>
          <ul className="c-tooltip-list">
            {props.internalLinks.map((link: InternalLinkDefinition, i) => (
              <li className="c-tooltip-list-item" key={i}>
                <a className="c-tooltip-link-viewpoint" href="#" onClick={() => gotoViewPoint(link)}>
                  {" "}
                  <Title viewpoint={viewpointsIndex[link.viewpointId]} data={link.params} />
                </a>
              </li>
            ))}
          </ul>
        </section>
      ) : null}
      {(props.externalLinks || []).length > 1 ? (
        <section className="c-tooltip-group">
          <h4 className="c-tooltip-list-title"><Trans>To know more...</Trans></h4>
          <ul className="c-tooltip-list">
            {props.externalLinks.map((link: ExternalLinkDefinition, i) => (
              <li className="c-tooltip-list-item" key={i}>
                <a className="c-tooltip-link-external" href={link.url}>
                  {" " + link.label}
                </a>
              </li>
            ))}
          </ul>
        </section>
      ) : null}
    </article>
  );
};
