import React from "react";
import { PlainObject } from "../../utils";

export type DataTableColumn = {
  id: string;
  label?: string;
};

export type DataTableProps = {
  columns: DataTableColumn[];
  data: PlainObject[];
};

export const DataTable: React.FC<DataTableProps> = (props: DataTableProps) => {
  const { data, columns } = props;

  return (
    <div className="c-datatable">
      <table cellSpacing="0">
        <thead>
          <tr>
            {columns.map((col) => (
              <th key={col.id}>{col.label || col.id}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {data.map((row, i) => (
            <tr key={i}>
              {columns.map((col) => (
                <td key={col.id}>{row[col.id]}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
