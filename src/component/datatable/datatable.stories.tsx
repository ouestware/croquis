import React from "react";
import { DataTable } from "./datatable";
import { range } from "lodash";
import * as faker from "faker";

import "../../scss/index.scss";

export default { title: "DataTable" };

export const basicExample = () => (
  <DataTable
    columns={[
      { id: "first", label: "First name" },
      { id: "last", label: "Last name" },
    ]}
    data={[
      { first: "Wendi", last: "Lebarree" },
      { first: "Nicky", last: "Wildes" },
      { first: "Cecilio", last: "Ickovic" },
      { first: "Fay", last: "Talks" },
      { first: "Torrin", last: "Broderick" },
    ]}
  />
);

export const largeExample = () => (
  <div
    style={{
      padding: "1em",
      maxWidth: 800,
      margin: "auto",
    }}
  >
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non finibus metus, et efficitur orci. Quisque magna
      velit, tempor in ante lobortis, consectetur auctor justo. Donec eros purus, aliquam a sapien tincidunt, placerat
      mollis nunc. Nulla odio sem, condimentum eget lacus at, condimentum consequat risus. Donec dapibus, lorem ac
      egestas vestibulum, quam enim consectetur tellus, nec aliquet velit nibh eget eros. Fusce eget lacus erat. Cras
      laoreet tempor ante at malesuada. Nam egestas dictum leo, rhoncus congue lectus efficitur nec. Quisque blandit ex
      purus. Cras velit quam, auctor maximus mauris vel, iaculis vehicula neque. Quisque quis urna porta nunc
      sollicitudin semper.
    </p>
    <DataTable
      columns={[
        { id: "firstName", label: "First name" },
        { id: "lastName", label: "Last name" },
        { id: "zipCode", label: "Zip code" },
        { id: "city", label: "City" },
        { id: "streetAddress", label: "Street address" },
        { id: "streetName", label: "Street name" },
        { id: "secondaryAddress", label: "Secondary address" },
        { id: "county", label: "County" },
        { id: "country", label: "Country" },
        { id: "countryCode", label: "Country code" },
        { id: "state", label: "State" },
        { id: "latitude", label: "Latitude" },
        { id: "longitude", label: "Longitude" },
        { id: "department", label: "Department" },
        { id: "companyName", label: "Company name" },
        { id: "companySuffix", label: "Company suffix" },
      ]}
      data={range(2000).map(() => ({
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        zipCode: faker.address.zipCode(),
        city: faker.address.city(),
        streetAddress: faker.address.streetAddress(),
        streetName: faker.address.streetName(),
        secondaryAddress: faker.address.secondaryAddress(),
        county: faker.address.county(),
        country: faker.address.country(),
        countryCode: faker.address.countryCode(),
        state: faker.address.state(),
        latitude: faker.address.latitude(),
        longitude: faker.address.longitude(),
        department: faker.commerce.department(),
        companyName: faker.company.companyName(),
        companySuffix: faker.company.companySuffix(),
      }))}
    />
    <p>
      Integer egestas lorem nisl, quis euismod ligula tincidunt eget. Sed luctus auctor ornare. Donec metus purus,
      porttitor ac mollis eget, faucibus nec libero. Suspendisse ullamcorper varius lectus et pellentesque. Aenean
      feugiat risus pretium tortor consequat convallis. Praesent eget ex risus. Praesent tempor vulputate pulvinar. Sed
      ullamcorper nunc eros, id maximus neque elementum quis. Etiam vitae lorem ex. Aliquam euismod ante eget mi
      bibendum, ut fermentum metus venenatis. Vestibulum bibendum semper aliquet.
    </p>
  </div>
);
