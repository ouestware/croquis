import React, { FC } from "react";

import "../../scss/index.scss";
import withSize, { SizeState } from "./withsize";

export default { title: "withSize (higher-order component)" };

export const basicExample = () => {
  const Component: FC<SizeState> = (props: SizeState) => (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        height: "100%",
        border: "1px solid #ccc",
      }}
    >
      <p>This component knows its size.</p>
      <p>
        It is{" "}
        <strong>
          {props.width}x{props.height}
        </strong>{" "}
        pixels.
      </p>
      <p>Try resizing this window to see it live updated.</p>
    </div>
  );
  const ComponentWithSize = withSize<SizeState>(Component);

  return <ComponentWithSize />;
};
