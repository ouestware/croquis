import React from "react";
import { Form } from "./form";

import "../../scss/index.scss";

export default { title: "Form" };

export const basicExample = () => (
  <Form
    viewpoint={{
      parameters: [
        { id: "country", label: "Pays", type: "string", required: true },
        { id: "year1", label: "Année de départ", type: "number", required: true, minimum: 1800, maximum: 2020 },
        { id: "year2", label: "Année de fin", type: "number", required: true, minimum: 1800, maximum: 2020 },
      ],
    }}
  />
);

export const withInitialData = () => (
  <Form
    viewpoint={{
      parameters: [
        { id: "country", label: "Pays", type: "string", required: true },
        { id: "year1", label: "Année de départ", type: "number", required: true, minimum: 1800, maximum: 2020 },
        { id: "year2", label: "Année de fin", type: "number", required: true, minimum: 1800, maximum: 2020 },
      ],
    }}
    initialData={{
      country: "France",
      somethingIrrelevant: "42",
    }}
  />
);

export const noParameter = () => <Form viewpoint={{ parameters: [] }} />;
