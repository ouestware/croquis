import React from "react";
import { WidgetProps } from "react-jsonschema-form";
import { pick, mapKeys } from "lodash";

import { PlainObject } from "../../utils";

const TYPES_MAP: PlainObject<string> = {
  string: "string",
  number: "number",
  integer: "number",
};

const ATTRIBUTES_MAP: PlainObject<string> = {
  minimum: "min",
  maximum: "max",
  multipleOf: "step",
};

export const Input: React.FC<WidgetProps> = (props: WidgetProps) => (
  <input
    className="c-form-input"
    onChange={(e) => props.onChange(e.target.value)}
    value={props.value || ""}
    type={TYPES_MAP[props.schema.type as string]}
    {...pick(props, ["id", "required"])}
    {...mapKeys(pick(props.schema, Object.keys(ATTRIBUTES_MAP)), (_, key) => ATTRIBUTES_MAP[key as string])}
  />
);

export const TextArea: React.FC<WidgetProps> = (props: WidgetProps) => (
  <textarea
    className="c-form-input"
    onChange={(e) => props.onChange(e.target.value)}
    value={props.value || ""}
    {...pick(props, ["id", "required"])}
  />
);

export default {
  TextWidget: Input,
  TextareaWidget: TextArea,
};
