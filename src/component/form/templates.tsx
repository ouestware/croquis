import React from "react";
import { FieldTemplateProps, ObjectFieldTemplateProps } from "react-jsonschema-form";
import cx from "classnames";
import { useTranslation } from "react-i18next";

export const ErrorsList: React.FC<{ errors: string[] }> = ({ errors }: { errors: string[] }) =>
  errors && errors.length ? (
    <ul className="c-form-errors-list">
      {errors.map((error, i) => (
        <li key={i}>{error}</li>
      ))}
    </ul>
  ) : null;

export const ObjectFieldTemplate: React.FC<ObjectFieldTemplateProps> = (props: ObjectFieldTemplateProps) => (
  <>{props.properties.map((element) => element.content)}</>
);

export const FieldTemplate: React.FC<FieldTemplateProps> = (props: FieldTemplateProps) => {
  const { t } = useTranslation();

  if (props.schema.properties) {
    return <div className="c-form-inputs">{props.children}</div>;
  }

  return (
    <div className={cx("c-form-row", props.classNames)}>
      <label className="c-form-label" htmlFor={props.id}>
        <span>
          {props.label}
          {props.required ? (
            <strong>
              {"\u00a0"}
              <abbr title={t("A value is required")}>*</abbr>
            </strong>
          ) : null}
        </span>
      </label>
      <div className="c-form-input-block">
        {props.description}
        {props.children}
        {props.rawErrors && <ErrorsList errors={props.rawErrors} />}
        {props.help}
      </div>
    </div>
  );
};
