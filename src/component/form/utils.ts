import { pick, pickBy } from "lodash";
import { JSONSchema6, JSONSchema6Definition } from "json-schema";

import { ViewPointDefinition } from "../../types";
import { PlainObject } from "../../utils";

export function getJSONSchema(viewpoint: ViewPointDefinition): JSONSchema6 {
  return {
    type: "object",
    required: viewpoint.parameters.filter((param) => param.required).map((param) => param.id),
    properties: viewpoint.parameters.reduce((iter, param): PlainObject<JSONSchema6Definition> => {
      return {
        ...iter,
        [param.id]: {
          title: param.label,
          ...pickBy(pick(param, ["type", "minimum", "maximum", "multipleOf"])),
        },
      };
    }, {}),
  };
}
