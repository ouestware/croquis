import React, { useState } from "react";
import { withTheme } from "react-jsonschema-form";
import { Trans } from "react-i18next";

import { ViewPointDefinition } from "../../types";
import { PartialButFor, PlainObject } from "../../utils";
import { getJSONSchema } from "./utils";
import theme from "./theme";

export type FormProps = {
  viewpoint: PartialButFor<ViewPointDefinition, "parameters">;
  initialData?: PlainObject;
  onSubmit?: (formState: PlainObject) => any;
  onCancel?: () => any;
};

const ThemedForm = withTheme(theme);

export const Form: React.FC<FormProps> = (props: FormProps) => {
  const { viewpoint, onSubmit, onCancel } = props;
  const [formState, setFormState] = useState<PlainObject>(props.initialData || {});

  if (!viewpoint || !viewpoint.parameters || !viewpoint.parameters.length) return null;

  const schema = getJSONSchema(viewpoint as ViewPointDefinition);

  return (
    <ThemedForm
      className="c-form"
      schema={schema}
      formData={formState}
      onChange={(e) => setFormState(e.formData)}
      onSubmit={onSubmit && (() => onSubmit(formState))}
    >
      <p className="c-form-actions">
        <button className="c-form-cancel" type="reset" onClick={onCancel}>
          ↑ <Trans>Cancel</Trans>
        </button>
        <button className="c-form-submit" type="submit">
          → <Trans>Submit</Trans>
        </button>
      </p>
    </ThemedForm>
  );
};
