import { ThemeProps } from "react-jsonschema-form";
import widgets from "./widgets";
import { FieldTemplate, ObjectFieldTemplate } from "./templates";

const theme: ThemeProps = {
  widgets: widgets,
  FieldTemplate: FieldTemplate,
  ObjectFieldTemplate: ObjectFieldTemplate,
  showErrorList: false,
};

export default theme;
