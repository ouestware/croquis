import React from "react";
import { Trans } from "react-i18next";

import pkg from "../../../package.json";

export const Footer: React.FC = () => {
  return (
    <footer className="c-footer">
      <Trans>
        Made with <span className="c-footer-colored">♥</span> with{" "}
        <a className="c-footer-colored c-footer-logo" href={pkg.homepage}>
          Croquis
        </a>{" "}
        by{" "}
        <a className="c-footer-colored" href="https://www.ouestware.com">
          OuestWare
        </a>
      </Trans>
    </footer>
  );
};
