import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Trans, useTranslation } from "react-i18next";

import { ViewPointDefinition } from "../../types";
import { getViewpointURL, PartialButFor } from "../../utils";
import { Title } from "../title";
import { Form } from "../form";

export type ViewPointProps = PartialButFor<ViewPointDefinition, "id" | "title">;

export const ViewPoint: React.FC<ViewPointProps> = (viewpoint: ViewPointProps) => {
  const history = useHistory();
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const { t } = useTranslation();

  const hasForm = !!viewpoint.parameters && !!viewpoint.parameters.length;

  function gotoViewPoint(formData?: any): void {
    const url = getViewpointURL(viewpoint, formData);

    // For cases where history isn't properly initialized
    if (!history)
      console.log(`[ViewPoint] Form submitted for viewpoint ${viewpoint.id} with payload`, formData, `(url: ${url})`);
    else history.push(url);
  }

  return (
    <article className={isOpen ? "c-viewpoint c-viewpoint-deployed" : "c-viewpoint"}>
      <div className="c-viewpoint-sumup">
        {viewpoint.image && (
          <img className="c-viewpoint-thumbnail" src={viewpoint.image} alt={t("Viewpoint thumbnail")} />
        )}
        <div className="c-viewpoint-text">
          <h3 className="c-viewpoint-title">
            <Title
              viewpoint={viewpoint}
              renderParam={(string) => <i className="c-inline-param-placeholder">{string}</i>}
            />
          </h3>
          {viewpoint.description && <p className="c-viewpoint-description">{viewpoint.description}</p>}

          {hasForm ? (
            <button className="c-viewpoint-deploy" onClick={() => setIsOpen(!isOpen)}>
              <Trans>Open</Trans>
            </button>
          ) : (
            <a
              className="c-viewpoint-goto"
              href="#"
              onClick={(e) => {
                gotoViewPoint();
                e.preventDefault();
              }}
            >
              <Trans>Go to viewpoint</Trans>
            </a>
          )}
        </div>
      </div>

      {hasForm && (
        <Form viewpoint={viewpoint as ViewPointDefinition} onSubmit={gotoViewPoint} onCancel={() => setIsOpen(false)} />
      )}
    </article>
  );
};
