import React from "react";
import { ViewPoint } from "./viewpoint";

import "../../scss/index.scss";

export default { title: "ViewPoint" };

export const basicExample = () => (
  <ViewPoint id="test" title="Commerce total de la France sur la totalité de la période" />
);

export const withImage = () => (
  <ViewPoint
    id="test"
    title="Commerce total de la France sur la totalité de la période"
    image="http://ricardo.medialab.sciences-po.fr/img/home_source_details.png"
  />
);

export const withDescription = () => (
  <ViewPoint
    id="test"
    title="Commerce total de la France sur la totalité de la période"
    image="http://ricardo.medialab.sciences-po.fr/img/home_source_details.png"
    description="Dans ce point de vue, le commerce total est égal à la somme des flux exportations (importations) avec toutes les entités partenaires."
  />
);

export const withParameters = () => (
  <ViewPoint
    id="test"
    title="Commerce total de ${country} de ${startDate} à ${endDate}"
    image="http://ricardo.medialab.sciences-po.fr/img/home_source_details.png"
    description="Dans ce point de vue, le commerce total est égal à la somme des flux exportations (importations) avec toutes les entités partenaires."
    parameters={[
      { id: "country", label: "Pays", type: "string" },
      { id: "startDate", label: "Date de début", type: "integer", minimum: 1700 },
      { id: "endDate", label: "Date de fin", type: "integer", maximum: 2000 },
    ]}
  />
);
