import React from "react";
import { Title } from "./title";

import "../../scss/index.scss";

export default { title: "Title" };

export const noParam = () => (
  <h3 className="c-title">
    <Title viewpoint={{ title: "Vue d'ensemble du corpus" }} />
  </h3>
);

export const withParams = () => (
  <h3 className="c-title">
    <Title
      viewpoint={{
        title: "Commerce total de ${country} de ${startDate} à ${endDate}",
        parameters: [
          { id: "country", label: "Pays", type: "string" },
          { id: "startYear", label: "Année de début", type: "integer" },
          { id: "endYear", label: "Date de fin", type: "integer" },
        ],
      }}
    />
  </h3>
);

export const withCustomParamsRendering = () => (
  <h3 className="c-title">
    <Title
      viewpoint={{
        title: "Commerce total de ${country} de ${startDate} à ${endDate}",
        parameters: [
          { id: "country", label: "Pays", type: "string" },
          { id: "startYear", label: "Année de début", type: "integer" },
          { id: "endYear", label: "Date de fin", type: "integer" },
        ],
      }}
      renderParam={(string) => <i className="c-inline-param-placeholder">{string}</i>}
    />
  </h3>
);

export const withData = () => (
  <h3 className="c-title">
    <Title
      viewpoint={{
        title: "Commerce total de ${country} de ${startDate} à ${endDate}",
        parameters: [
          { id: "country", label: "Pays", type: "string" },
          { id: "startYear", label: "Année de début", type: "integer" },
          { id: "endYear", label: "Date de fin", type: "integer" },
        ],
      }}
      data={{
        country: "France",
        startDate: 1877,
        endDate: 1942
      }}
    />
  </h3>
);
