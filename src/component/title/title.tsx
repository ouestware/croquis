import React from "react";
import { keyBy, mapValues } from "lodash";
import { PartialButFor, PlainObject } from "../../utils";
import { ViewPointDefinition } from "../../types";

export type TitleProps = {
  viewpoint: PartialButFor<ViewPointDefinition, "title">;
  data?: PlainObject;
  renderParam?: React.FC<string>;
};

const DefaultRenderParam: React.FC<string> = (string) => <i className="c-inline-param">{string}</i>;

/**
 * This regex will split a title string on `${param}` like sub-strings, to
 * return an array with odd elements being the strings between the params, and
 * even elements being the param IDs themselves.
 *
 * Example:
 * > "${p1} lorem ipsum ${p2}${p3}".split(SPLITTER);
 * returns
 * > ["","p1"," lorem ipsum ","p2","","p3",""]
 */
const SPLITTER = /\${ *([^ }]*) *}/g;

export const Title: React.FC<TitleProps> = (props: TitleProps) => {
  const { viewpoint, data, renderParam = DefaultRenderParam } = props;

  const paramsDict = data || mapValues(keyBy(viewpoint.parameters, "id"), (param) => param?.label?.toLowerCase());

  return (
    <>
      {viewpoint.title.split(SPLITTER).map((string, i) => (
        <React.Fragment key={i}>{i % 2 ? renderParam((paramsDict[string] || string) + "") : string}</React.Fragment>
      ))}
    </>
  );
};

export function getStringTitle(props: TitleProps): string {
  const { viewpoint, data } = props;

  const paramsDict = data || mapValues(keyBy(viewpoint.parameters, "id"), (param) => param?.label?.toLowerCase());

  return viewpoint.title
    .split(SPLITTER)
    .map((string, i) => (i % 2 ? (paramsDict[string] || string) + "" : string))
    .join("");
}
