import React, { ComponentType } from "react";
import { ComponentDefinition } from "../types";
import { Table } from "../data/datasources";

// List all components:
import { DataGrid } from "./datagrid";
import { VegaChart } from "./vegachart";
import { PlainObject } from "../utils";

export const components: { [id: string]: ComponentType<any> } = {
  datagrid: DataGrid,
  vega: VegaChart,
};

interface Properties {
  data: PlainObject<Table>;
  params: PlainObject;
}

export const DataComponent: React.FC<ComponentDefinition & Properties> = (props: ComponentDefinition & Properties) => {
  const { id, title, data, params, type, options } = props;

  // Find the chart:
  const Chart = components[type];
  if (!Chart) {
    throw new Error(`Chart of type "${type}" not found`);
  }

  return (
    <div className="c-chartblock">
      <h3 className="c-chartblock-title">
        <a href={"#" + id} className="c-chartblock-title-anchor" />
        {title}
      </h3>
      <Chart data={data} params={params} options={options} />
    </div>
  );
};
