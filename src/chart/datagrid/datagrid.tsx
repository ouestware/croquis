import React from "react";
import { Table } from "../../data/datasources";
import { useTable } from "../../data/hooks";
import { DataTable } from "../../component/datatable";
import { PlainObject } from "../../utils";
import { Trans } from "react-i18next";

interface Properties {
  data: PlainObject<Table>;
  params: PlainObject;
  options: any;
}
export const DataGrid: React.FC<Properties> = (props: Properties) => {
  const { loading, data } = useTable(props.data.table, props.params);

  if (loading) return <h2><Trans>Loading</Trans></h2>;
  if (!data || data.length === 0) return null;

  return (
    <DataTable columns={props.data.table.columns().map((col) => ({ id: col.name, label: col.name }))} data={data} />
  );
};
