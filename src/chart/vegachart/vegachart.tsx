import React from "react";
import { VegaLite } from "react-vega";
import { Table } from "../../data/datasources";
import { useData } from "../../data/hooks";
import withSize, { SizeState } from "../../component/withsize/withsize";
import { TopLevelSpec } from "vega-lite";
import { PlainObject } from "../../utils";

interface VegaChartProps extends SizeState {
  data: PlainObject<Table>;
  params: PlainObject;
  options: {
    spec: TopLevelSpec;
  };
}

const WrappedVegaChart: React.FC<VegaChartProps> = (props: VegaChartProps) => {
  const spec = props.options.spec;
  const { loading, data } = useData(props.data, props.params);

  return (
    <div style={{ height: 400 }}>
      {loading && <h2>Loading</h2>}
      {!loading && props.isMounted ? (
        <VegaLite
          data={data}
          width={props.width}
          height={props.height}
          actions={false}
          spec={{
            ...spec,
            autosize: {
              type: "fit",
              resize: true,
              contains: "padding",
            },
            config: {
              ...spec.config,
              font: "'Open Sans', sans-serif",
            },
          }}
        />
      ) : null}
    </div>
  );
};

export const VegaChart = withSize<VegaChartProps>(WrappedVegaChart);
