import "./scss/index.scss";
// React
import React from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";
// Routing system
import { BrowserRouter as Router } from "react-router-dom";
import { RouterWrapper } from "./router/router";
import { routes } from "./router/routes";
// Internationalization
import "./i18n";

ReactDOM.render(
  <Router>
    <RouterWrapper routes={routes} />
  </Router>,
  document.getElementById("root"),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
