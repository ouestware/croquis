import cx from "classnames";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { ViewPointDefinition } from "../../types";
import { getViewpointURL, PartialButFor, PlainObject } from "../../utils";
import { Title } from "../../component/title";
import { Form } from "../../component/form";

export type BannerProps = {
  viewpoint: PartialButFor<ViewPointDefinition, "id" | "title">;
  data: PlainObject;
};

export const Banner: React.FC<BannerProps> = ({ viewpoint, data }: BannerProps) => {
  const history = useHistory();
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const hasForm = !!viewpoint.parameters && !!viewpoint.parameters.length;

  function gotoViewPoint(formData?: any): void {
    const url = getViewpointURL(viewpoint, formData);

    // For cases where history isn't properly initialized
    if (!history)
      console.log("[Banner] Form submitted for viewpoint ${viewpoint.id} with payload", formData, `(url: ${url})`);
    else history.push(url);
  }

  return (
    <section className={cx("c-banner", isOpen && "c-banner-deployed")}>
      <h1 className="c-banner-title" onClick={() => setIsOpen(true)}>
        <a
          href="#"
          className="c-banner-title-anchor"
          onClick={(e) => {
            setIsOpen(!isOpen);
            e.preventDefault();
            e.stopPropagation();
          }}
        />
        <Title viewpoint={viewpoint} data={data} renderParam={(string) => <i className="c-inline-param">{string}</i>} />
      </h1>

      {hasForm && (
        <Form
          viewpoint={viewpoint as ViewPointDefinition}
          initialData={data}
          onSubmit={gotoViewPoint}
          onCancel={() => setIsOpen(false)}
        />
      )}
    </section>
  );
};
