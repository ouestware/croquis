import React, { useEffect } from "react";
import cx from "classnames";
import { Link } from "react-router-dom";
import { Trans, useTranslation } from "react-i18next";

import { ComponentDefinition, ViewPointDefinition } from "../../types";
import { DataComponent } from "../../chart";
import { DataFactory } from "../../data";
import { Table } from "../../data/datasources";
import { Banner } from "./banner";
import { PlainObject } from "../../utils";
import { Footer } from "../../component/footer";
import logo from "../../assets/img/logo.svg";
import { getStringTitle } from "../../component/title";
import { Configuration } from "../../config";

interface Props {
  configuration: Configuration;
  viewpoint: ViewPointDefinition;
  params: PlainObject;
}

/**
 * Page component that display a viewpoint.
 */
export const ViewPointPage: React.FC<Props> = ({ configuration, viewpoint, params }: Props) => {
  const { t } = useTranslation();
  useEffect(() => {
    document.title = `${configuration.metadata?.title || t("Croquis")} | ${getStringTitle({
      viewpoint,
      data: params,
    })}`;
  });

  // building all tables for this vp
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  const tables: { [key: string]: Table } = {};
  viewpoint.components
    .flatMap((component) => component.tables)
    .forEach((tableref) => {
      if (!tables[tableref.id]) {
        tables[tableref.id] = DataFactory.getTable(tableref.id);
      }
    });

  return (
    <div className={cx("c-viewpointpage", viewpoint.layout)}>
      <header className="c-viewpointpage-header">
        <Link to="/" className="c-viewpointpage-header-title">
          <img src={logo} alt={t("Croquis logo")} />
          <h2>
            <Trans>Croquis</Trans>
          </h2>
        </Link>
      </header>

      <Banner viewpoint={viewpoint} data={params} />

      <main>
        {viewpoint.components.map((def: ComponentDefinition, index) => {
          // compute tables
          const componentTables: { [alias: string]: Table } = {};
          def.tables.forEach((item) => {
            componentTables[item.alias || item.id] = tables[item.id];
          });
          return <DataComponent key={index} data={componentTables} params={params} {...def} />;
        })}
      </main>

      <Footer />
    </div>
  );
};
