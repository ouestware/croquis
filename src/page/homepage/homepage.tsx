import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { Trans, useTranslation } from "react-i18next";

import { ViewPointDefinition } from "../../types";
import { ViewPoint } from "../../component/viewpoint";
import { Footer } from "../../component/footer";
import { Configuration } from "../../config";
import logo from "../../assets/img/logo.svg";

export const HomePage: React.FC<{ configuration: Configuration }> = ({
  configuration,
}: {
  configuration: Configuration;
}) => {
  const { t } = useTranslation();
  useEffect(() => {
    document.title = `${configuration.metadata?.title || t("Croquis")} | ${t("Homepage")}`;
  });

  return (
    <div className="c-homepage">
      <header className="c-homepage-header">
        <Link to="/" className="c-homepage-header-banner">
          <img src={logo} alt={t("Croquis logo")} />
          <h1>{configuration.metadata?.title || t("Croquis")}</h1>
        </Link>
        {configuration.metadata?.subtitle && <h2>{configuration.metadata.subtitle}</h2>}
      </header>

      <main>
        <h2>
          <Trans>List of available views</Trans>
        </h2>
        {configuration.viewpoints.map((viewpoint: ViewPointDefinition) => {
          return <ViewPoint key={viewpoint.id} {...viewpoint} />;
        })}
      </main>

      <Footer />
    </div>
  );
};
