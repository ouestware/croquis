import { DataSourcesConfigurationType, TablesConfigurationType } from "./data/datasources";
import { PlainObject } from "./utils";

export type DataSourceDefinition = DataSourcesConfigurationType & {
  id: string;
  type: string;
};

export type TableDefinition = TablesConfigurationType & {
  id: string;
  datasource: string;
};

export type ParameterType = string;

export interface ParameterDefinition {
  id: string;
  type: ParameterType;
  label?: string;
  required?: boolean;
  defaultValue?: string;
  placeholder?: string;

  // Constraints defined as in react-jsonschema-form:
  minimum?: number;
  maximum?: number;
  multipleOf?: number;
}

export interface ViewPointDefinition {
  id: string;
  title: string;
  description?: string;
  image?: string;
  layout?: string;
  parameters: Array<ParameterDefinition>;
  components: Array<ComponentDefinition>;
}

export interface ComponentDefinition {
  id: string;
  title: string;
  tables: Array<{ id: string; alias?: string }>;
  action: (params: any) => void;
  type: string;
  options?: any;
}

export interface InternalLinkDefinition {
  viewpointId: string;
  params: PlainObject;
}

export interface ExternalLinkDefinition {
  url: string;
  label: string;
}
