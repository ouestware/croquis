= Project Croquis

image:https://gitlab.com/ouestware/croquis/badges/devel/pipeline.svg[link="https://gitlab.com/ouestware/croquis/pipelines/latest",title="pipeline status"]
image:https://gitlab.com/ouestware/croquis/badges/devel/coverage.svg[link="https://gitlab.com/ouestware/croquis/pipelines/latest",title="coverage report"]


This project was bootstrapped with https://github.com/facebook/create-react-app[Create React App].

== Available Scripts

In the project directory, you can run:

=== `yarn start`

Runs the app in the development mode.
Open http://localhost:3000 to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

=== `yarn test`

Launches the test runner in the interactive watch mode.
See the section about https://facebook.github.io/create-react-app/docs/running-tests[running tests] for more information.

IMPORTANT: All tests with a name that doesn't contain `[INTEG]` will be executed.
If you want to run all of them, yo should use `yarn test:all`

=== `yarn build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

See the section about https://facebook.github.io/create-react-app/docs/deployment[deployment] for more information.

=== `yarn eject`

NOTE: this is a one-way operation. Once you `eject`, you can’t go back!

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

=== `yarn storybook`

Displays a nice UI with all stories for the various reusable components of Croquis, using https://www.learnstorybook.com/[StoryBook].
It aims to replace components examples, documentation and rendering/interaction tests.

=== Localisation

We use https://www.i18next.com[i18next] to manage translations. The JSON translations files are located in the `src/locales` directory.
Run `yarn extract-i18n` to extract keys from the code and add them to the existing JSON translations files.

== Learn More

You can learn more in the https://facebook.github.io/create-react-app/docs/getting-started[Create React App documentation].

To learn React, check out the https://reactjs.org/[React documentation].
